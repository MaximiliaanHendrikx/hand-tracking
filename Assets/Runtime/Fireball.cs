﻿using System;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    [SerializeField] private float _speedMultiplier = 5f;
    private Vector3 _velocity;

    private void Update()
    {
        transform.position += _speedMultiplier * Time.deltaTime * _velocity;
    }

    public void Shoot(Vector3 velocity)
    {
        _velocity = velocity;
    }
}