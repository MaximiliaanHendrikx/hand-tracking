﻿using DG.Tweening;
using UnityEngine;

public abstract class Spell : MonoBehaviour
{
    [SerializeField] private GameObject _spellVisual;

    private bool _isEnabled = false;

    private void Start()
    {
        Disable();
    }

    public virtual void Enable()
    {
        if (_isEnabled)
            return;

        _spellVisual.SetActive(true);

        gameObject.transform.localScale = new Vector3(1f, 0f, 1f);
        gameObject.transform.DOScaleY(1f, 0.35f).SetEase(Ease.InOutQuad);

        _isEnabled = true;
    }
    public virtual void Disable()
    {
        if (!_isEnabled)
            return;

        gameObject.transform.DOScaleY(0f, 0.15f)
            .SetEase(Ease.InOutQuad)
            .OnComplete(OnCompleteDisable);
    }

    public virtual void OnCompleteDisable()
    {
        _spellVisual.SetActive(false);
        _isEnabled = false;
    }
}
