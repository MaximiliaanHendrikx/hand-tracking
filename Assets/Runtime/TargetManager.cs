﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TargetManager : MonoBehaviour
{
    [SerializeField] private float _timeBetweenWaves = 5f;

    private List<TargetSpawner> _targetSpawners = new();

    private int _wave = 0;
    private float _timer = 0f;

    private void Awake()
    {
        foreach (Transform transform in transform)
        {
            _targetSpawners.Add(transform.GetComponent<TargetSpawner>());
        }
    }

    private void Update()
    {
        _timer += Time.deltaTime;

        if (_timer  >= _timeBetweenWaves)
        {
            _timer = 0;
            _wave++;

            int index = UnityEngine.Random.Range(0, _targetSpawners.Count);
            _targetSpawners[index].StartWave();
        }
    }
}