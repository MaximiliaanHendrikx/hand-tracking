﻿using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField] private Bullet _bullet;
    [SerializeField] private Transform _bulletSpawnTransform;
    public void Shoot()
    {
        Bullet bullet = Instantiate(_bullet, _bulletSpawnTransform.position, Quaternion.identity);
        bullet.Setup(_bulletSpawnTransform.forward, 5f);
    }
}
