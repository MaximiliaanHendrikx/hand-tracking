﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    private float _speed;
    private Vector3 _velocity;

    public void Setup(Vector3 velocity, float speed)
    {
        _velocity = velocity;
        _speed = speed;
    }

    private void Update()
    {
        transform.position += _velocity * _speed * Time.deltaTime;
    }
}