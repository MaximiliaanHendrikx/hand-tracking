﻿using Oculus.Interaction.Input;
using UnityEngine;

public class TargetSpawner : MonoBehaviour
{
    [SerializeField] private Target _targetPrefab;

    private float _timer = 0f;
    private float _timeBetweenTargets = 0.4f;

    private int _targetsToSpawn = 10;
    private int _spawnedTargets = 0;

    private bool _isWavePlaying = false;

    private Transform _targetJumpToTransform;

    private void Awake()
    {
        _targetJumpToTransform = FindObjectOfType<Hmd>().transform;
    }

    private void Update()
    {
        if (!_isWavePlaying)
            return;

        _timer += Time.deltaTime;

        if (_timer >= _timeBetweenTargets)
            SpawnTarget();

        if (_spawnedTargets >= _targetsToSpawn)
            _isWavePlaying = false;
    }

    private void SpawnTarget()
    {
        Target spawnedTarget = Instantiate(_targetPrefab, transform.position, Quaternion.identity);
        spawnedTarget.JumpToPosition(_targetJumpToTransform.position);
        _spawnedTargets++;
        _timer = 0f;
        _timeBetweenTargets = Random.Range(0.3f, 0.55f);
    }

    public void StartWave()
    {
        _timer = 0f;
        _spawnedTargets = 0;
        _targetsToSpawn = Random.Range(5, 15);
        _isWavePlaying = true;
    }
}
