﻿using UnityEngine;
using DG.Tweening;

public class Target : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        transform.DOScale(0f, 0.1f)
            .OnComplete(() => Destroy(gameObject));
    }
    public void JumpToPosition(Vector3 toPosition) => transform.DOJump(toPosition + Random.insideUnitSphere / 2, 1f, 1, 2.5f).OnComplete(() => Destroy(gameObject));
}