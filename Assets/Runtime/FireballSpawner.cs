using System.Collections;
using UnityEngine;
using DG.Tweening;

public class FireballSpawner : MonoBehaviour
{
    [SerializeField] private Fireball _fireballPrefab;
    [SerializeField] private Transform _spawnTransform;

    [Header("DoTween Settings")]
    [SerializeField] private float _scaleTime = 1f;

    private Fireball _summonedFireball;
    private bool _isFireballSummoned;

    public void SummonFireball()
    {
        if (!_isFireballSummoned)
        {
            _summonedFireball = Instantiate(_fireballPrefab, _spawnTransform.position, Quaternion.identity);
            _summonedFireball.transform.localScale = Vector3.zero;
            _summonedFireball.transform.DOScale(1f, _scaleTime);

            _isFireballSummoned = true;
        }
    }

    public void DesummonFireball()
    {
        Destroy(_summonedFireball.gameObject);
    }

    public void ShootFireball()
    {
        _summonedFireball.Shoot(_spawnTransform.forward);
        _summonedFireball = null;
    }
}
