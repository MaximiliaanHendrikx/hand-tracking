using System.Collections;
using UnityEngine;
public class GunManager : MonoBehaviour
{
    [SerializeField] private Gun _gun;

    public void EnableGun()
    {
        _gun.gameObject.SetActive(true);
    }

    public void DisableGun()
    {
        _gun.gameObject.SetActive(false);
    }
}
